package com.cafeteria.api.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.cafeteria.api.entity.EmbeddedKey.OrderKey;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="order_beverage")
public class OrderBeverage {
	
	@EmbeddedId
	private OrderKey id;
	
	@JsonBackReference
	@ManyToOne
	@MapsId("OrderId")
	@JoinColumn(name="OrderId")
	Order order;
	
	@JsonBackReference
	@ManyToOne
	@MapsId("BeverageId")
	@JoinColumn(name="BeverageId")
	Beverage beverage;

	public OrderKey getId() {
		return id;
	}

	public void setId(OrderKey id) {
		this.id = id;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Beverage getBeverage() {
		return beverage;
	}

	public void setBeverage(Beverage beverage) {
		this.beverage = beverage;
	}
	
}
