package com.cafeteria.api.entity;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="order")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="OrderId", nullable = false)
	private Integer orderId;
	
	@Column(name="OrderDate", nullable = false)
	private Date orderDate;
	
	@Column(name="OrderNote", nullable = false, length=1000)
	private String orderNote;
	
	@Column(name="OrderAddress", nullable = false, length=100)
	private String orderAddress;
	
	@Column(name="OrderTotal")
	private Integer orderTotal;
	
	@Column(name="OrderFinalTotal")
	private Float orderFinalTotal;
	
	@Column(name="VoucherId", nullable = false)
	private Integer voucherId;
	
	//-------- references ----------
	
	//@JsonManagedReference
	//@OneToMany(targetEntity=OrderBeverage.class, mappedBy="order", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	
	@ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinTable(name = "order_beverage",
				joinColumns = {
					@JoinColumn(name = "OrderId", referencedColumnName = "OrderId", nullable = false, updatable = false)
				},
				inverseJoinColumns = {
					@JoinColumn(name = "BeverageId", referencedColumnName = "BeverageId", nullable = false, updatable = false)
				})
	private Set<Beverage> orderBeverages;

	
	//-------- getter setter --------
	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderNote() {
		return orderNote;
	}

	public void setOrderNote(String orderNote) {
		this.orderNote = orderNote;
	}

	public String getOrderAddress() {
		return orderAddress;
	}

	public void setOrderAddress(String orderAddress) {
		this.orderAddress = orderAddress;
	}

	public Integer getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(Integer orderTotal) {
		this.orderTotal = orderTotal;
	}

	public Float getOrderFinalTotal() {
		return orderFinalTotal;
	}

	public void setOrderFinalTotal(Float orderFinalTotal) {
		this.orderFinalTotal = orderFinalTotal;
	}

	public Integer getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(Integer voucherId) {
		this.voucherId = voucherId;
	}

	public Set<Beverage> getOrderBeverages() {
		return orderBeverages;
	}

	public void setOrderBeverages(Set<Beverage> orderBeverages) {
		this.orderBeverages = orderBeverages;
	}
	
}
