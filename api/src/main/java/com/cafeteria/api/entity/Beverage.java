package com.cafeteria.api.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity(name="beverage")
@Table(name="beverage")
public class Beverage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="BeverageId", nullable = false)
	private Integer beverageId;
	
	@Column(name="BeverageSize", nullable = false)
	private Integer beverageSize;
	
	@Column(name="BeveragePrice", nullable = false)
	private Integer beveragePrice;
	
	
	//----- references -------
	@Column(name="CategoryId")
	private Integer categoryId;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="CategoryId", insertable= false, updatable= false)
	private Category category;
	
	/*@ManyToMany(mappedBy = "orderBeverages", fetch = FetchType.LAZY)
	private Set<Order> orders;
	
	public Set<Order> getOrders() {
		return orders;
	}

	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}*/

	//------getter setter------
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Integer getBeverageId() {
		return beverageId;
	}

	public void setBeverageId(Integer beverageId) {
		this.beverageId = beverageId;
	}

	public Integer getBeverageSize() {
		return beverageSize;
	}

	public void setBeverageSize(Integer beverageSize) {
		this.beverageSize = beverageSize;
	}

	public Integer getBeveragePrice() {
		return beveragePrice;
	}

	public void setBeveragePrice(Integer beveragePrice) {
		this.beveragePrice = beveragePrice;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	
}
