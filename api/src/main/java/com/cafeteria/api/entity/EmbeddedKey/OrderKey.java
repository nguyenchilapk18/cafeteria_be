package com.cafeteria.api.entity.EmbeddedKey;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3973622653313650366L;

	@Column(name = "OrderId")
	public Integer orderId;
	
	@Column(name = "BeverageId")
	public Integer beverageId;

	public OrderKey(Integer orderId, Integer beverageId) {
		this.orderId = orderId;
		this.beverageId = beverageId;
	}
	
	
}
