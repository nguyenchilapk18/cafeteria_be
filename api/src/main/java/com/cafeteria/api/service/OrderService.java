package com.cafeteria.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cafeteria.api.entity.Order;
import com.cafeteria.api.repository.OrderRepository;

@Service
public class OrderService {
	
	private OrderRepository orderRepo;
	
	@Autowired
	public OrderService(OrderRepository orderRepo) {
		this.orderRepo = orderRepo;
	}
		
	public List<Order> getAllOrders(){
		return orderRepo.findAll();
	}
	
	public Order getOrderById(Integer id) {
		return orderRepo.findById(id).orElse(null);
	}
	
	public Order addOrder(Order order) {
		return orderRepo.save(order);
	}
}
